

Part 1 – Architecture and Functionality
•	What is the Resiliency Tier (Gold/Platinum/Bronze)
•	What SLOs are trying to be met
•	What are the main technology stacks/components utilized (cloud/datacenter/jvm/db/queue/entry and exit points)
•	What does the service do
•	How does it do its intended function
o	What end points	
o	What end users
o	Are responses returned or received (or both)
o	What status codes are used
•	What business fuctions are provided
•	What are the consequences if the services are unavailable?
o	What other services are affected
o	Are employees impacted
o	Are customers impacted
•	What resiliency patterns are in place?  Multiple zones, etc

Part 2 – Dependeny Deep Dive
o	What are the dependencies of this service? (what services are required for this service to function)
o	Other microservice/app
o	DB
o	Batch
o	Cache
o	Network
o	Infrastructure
o	What are the consequences of each dependency being unavailable
o	Full down
o	Degraded
o	How do we test these scenarios
o	Does each dependency have measured SLO
o	What services are dependent on this app
o	what impact if your app is hard down
o	do you have circtuit breakers in place

Part 3 – Monitoring, Alerting, SLOs
o	What tools are used for monitoring
o	What SLOs are in place?
o	What SLIs are they based off of?
o	What time period are they measured against?
o	Is there an error budget?
o	Does the service have a health check?
o	Does the health check validate critical dependencies
o	Do we have Black Box monitoring?
o	Do we have White Box monitoring?
o	What alerts are defined?
o	What thresholds are set (time based or standard deviation?)
o	Do you track MTTD, MTTR
o	If so, do you track and report it
o	What does your monitoring look like when the service is degraded/down.
o	SOPs in place (playbooks)

Part 4 – Performance, Capacity, Destructive Testing
o	System specs for production/performance testing
o	Instances
o	Cpu
o	Memory
o	Heap
o	Threads
o	Db
o	Other
o	Average vs Peak load indicators (golden signals)
o	Current production load patterns
o	Projected additional load/forecasted load
o	Scaling needs and usage (downtime needed?)
o	Can we shed traffic or move users off of bad instances? Is this automated?  (LB?)
o	Have you planned destructive testing?
o	Is the current design appropriately sized for normal runtime, holidays, and growth patterns?
o	How does the service scale?
o	How does the service handle Network/Infra failures? 
o	Retry?
o	Caching
o	Redundant calls?
o	Wheel of Misfortune?

Part 5 – Release Engineering
o	Deployment strategy? 
o	Canary 
o	Blue/green
o	Beta?
o	How are releases certified?
o	Functional/QA?
o	Performance testing?
o	Stress testing?
o	Deployment?
o	Backout Testing?
o	Are there runtime activities that could affect the service?
o	Data loading
o	Batch jobs
o	Cache clears
o	Traffic routing
o	If cloud native, are you provisioning the current number of instances?
o	What costs?
o	Are resources being reviewed?
o	How often?


